# GDMPS frontend

## Project Start

安装依赖

```bash
yarn install
```

### Compiles and hot-reloads for development

项目运行

```bash
yarn serve
```

### Compiles and minifies for production

打包

```bash
yarn build
```

### Lints and fixes files

```bash
yarn lint
```

### 目录结构

- public // 资源目录和一些无需打包的资源，比如标题图片
- dist // 项目build输出文件
- node_modules // 依赖包
- src //源码目录
  - assets // 静态资源
  - components //全局公共组件
  - css //全局样式
  - icons // SVG图标
  - views // 页面目录
    - Admin //管理员端
    - Student //学生端
    - Teacher  //教师端
    - forgetPwd.vue //忘记密码页面
    - login.vue //登录页面
  - router //路由
  - services //请求封装
  - store //存储
  - utils //工具封装
  - .eslintigonore //eslint忽略配置
  - App.vue //入口
  - main.js //全局配置
- eslintrc.js //eslint配置
- bable.config.js //bable配置
- jsconfig.json //js配置
- package.json //依赖包配置
- vue.config.js //vue全局配置

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
