/**
 * 配置参考: https://cli.vuejs.org/zh/config/
 */
const CompressionPlugin = require('compression-webpack-plugin');//引入gzip压缩插件
const { defineConfig } = require('@vue/cli-service') // ts注解
// 是否是生产环境
const isProduction = process.env.NODE_ENV !== 'development';
console.log(isProduction)
// 是否需要使用cdn
const isNeedCdn = false
// cdn 加速文件
const assetsCDN = {
  /**
   * externals对象的属性名为package.json中，对应的库的名称（固定写法）,属性值为引入时你自定义的名称
   */
  externals: {
    'vue': 'Vue',
    'element-ui': 'ELEMENT',
    'axios': 'axios',
    'vue-router': 'VueRouter',
    'vuex': 'Vuex',
    // 'lodash': '_'
  },
  css: [
    'https://cdn.staticfile.org/element-ui/2.15.12/theme-chalk/index.min.css',
  ],
  js: [
    'https://cdn.staticfile.org/vue/2.7.14/vue.min.js',
    'https://cdn.staticfile.org/element-ui/2.15.12/index.min.js',
    'https://cdn.staticfile.org/axios/1.2.1/axios.min.js',
    'https://cdn.staticfile.org/vue-router/3.0.7/vue-router.min.js',
    'https://cdn.bootcss.com/vuex/3.6.2/vuex.min.js',
    // 'https://cdn.staticfile.org/lodash.js/4.17.21/lodash.min.js',
  ]
}
const plugins = [new CompressionPlugin({
  // asset: '[path].gz[query]',// 目标文件名
  // algorithm: 'gzip',// 使用gzip压缩
  test: /\.js$|\.html$|\.css/, //匹配文件名
  threshold: 10240, //大于10KB的文件进行压缩
  // minRatio: 0.8, // 最小压缩比达到0.8时才会被压缩
  deleteOriginalAssets: false //是否删除原文件
})]
module.exports = defineConfig({ 
  transpileDependencies: true,
  // configureWebpack: config => {
  //   if (process.env.NODE_ENV === 'production') {
  //     return {
  //       plugins: [new CompressionPlugin({
  //         test: /\.js$|\.html$|\.css/, //匹配文件名
  //         threshold: 10240, //大于10KB的文件进行压缩
  //         deleteOriginalAssets: false //是否删除原文件
  //       })]
  //     }
  //   }
  // },
  /**
  * 首页访问优化: 排除依赖使用cdn方式引入
  * 参考： https://blog.csdn.net/weixin_43919509/article/details/124149728
  * @param config
  */
  configureWebpack: {
    // 生产环境，则添加不参与打包的包名和依赖包的名称
    externals: (isProduction || isNeedCdn) ? assetsCDN.externals : {},
    plugins: (process.env.NODE_ENV === 'production') ? plugins : []
  },
  chainWebpack: config => {
    // 生产环境或需要cdn时，才注入cdn
    if (isProduction || isNeedCdn) {
      console.log('生产环境或需要cdn时，才注入cdn')
      config.plugin('html').tap(args => {
        args[0].cdn = assetsCDN
        return args
      })
    }
  },
  // 原文链接：https://blog.csdn.net/qq_41463655/article/details/125503250

  // 首页访问优化: 关闭生成的map.js 文件  https://blog.csdn.net/z591102/article/details/108076212
  productionSourceMap: false, //如果不想让别人看到源码可以设置为false，并且可以减少打包后包的体积，加密源码。
  // 【注】.map文件的作用：
  // 项目打包后，代码都是经过压缩加密的，如果运行时报错，输出的错误信息无法准确得知是哪里的代码报错。有了map就可以像未加密的代码一样，准确的输出是哪一行哪一列有错。
  
  // publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  // devServer: { 
  //   // 设置为0.0.0.0则所有的地址均能访问
  //   open: true,
  //   host: 'localhost',
  //   port: 8080,
  //   https: false,
  //   // 跨域问题解决 代理（关键部分）
  //   proxy: {
  //     '/api': {
  //       target: 'http://47.109.101.181:8082', //此处为后端提供的真实接口
  //       changeOrigin: true, // 允许跨域
  //       pathRewrite: {
  //         // 如果接口中是没有api的，那就直接置空，'^/api': ''，
  //         // 如果接口中有api，那就得写成{'^/api':'/api'}
  //         '^/api': ''
  //       }
  //     }
  //   },
  //   overlay: {
  //     errors: true,
  //     warnings: true
  //   }
  // },
})
//vue.config.js