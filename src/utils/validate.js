/**
 * 邮箱
 * @param {*} s
 */
 export function isEmail(s) {
  return /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(s)
}

/**
 * 手机号码
 * @param {*} s
 */
export function isMobile(s) {
  return /^1[0-9]{10}$/.test(s)
}

/**
 * 电话号码
 * @param {*} s
 */
export function isPhone(s) {
  return /^([0-9]{3,4}-)?[0-9]{7,8}$/.test(s)
}

/**
 * URL地址
 * @param {*} s
 */
export function isURL(s) {
  return /^http[s]?:\/\/.*/.test(s)
}

/**
 * 身份证校验
 */
export function checkIdCard(idcard) {
  const regIdCard = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
  if (!regIdCard.test(idcard)) {
    return false;
  } else {
    return true;
  }
}

/**
 * 是否合法IP地址
 * @param {*} rule
 * @param {*} value
 * @param {*} callback
 */
 export function validateIP(rule, value, callback) {
  if (value==''||value==undefined||value==null) {
    callback();
  } else {
    const reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
    if ((!reg.test(value)) && value != '') {
      callback(new Error('请输入正确的IP地址'));
    } else {
      callback();
    }
  }
}

/**
 * 校验数值范围
 * @param {*} rule
 * @param {*} value
 * @param {*} callback
 */
 export function checkMax100(rule, value, callback) {
  if (value == '' || value == undefined || value == null) {
    callback();
  } else if (!Number(value)) {
    callback(new Error('请输入[0,100]之间的数字'));
  } else if (value < 0 || value > 100) {
    callback(new Error('请输入[0,100]之间的数字'));
  } else {
    callback();
  }
}

/**
 * 验证输入框输入最大数值
 * @param {*} rule
 * @param {*} value
 * @param {*} callback
 */
 export function checkMaxVal(rule, value, callback) {
  if (value < 0 || value > 最大值) {
    callback(new Error('请输入[0,20000]之间的数字'));
  } else {
    callback();
  }
}

/**
 * 验证是否整数
 * @param {*} rule
 * @param {*} value
 * @param {*} callback
 */
 export function isInteger(rule, value, callback) {
  if (!value) {
    return callback(new Error('输入不可以为空'));
  }
  setTimeout(() => {
    if (!Number(value)) {
      callback(new Error('请输入正整数'));
    } else {
      const re = /^[0-9]*[1-9][0-9]*$/;
      const rsCheck = re.test(value);
      if (!rsCheck) {
        callback(new Error('请输入正整数'));
      } else {
        callback();
      }
    }
  }, 0);
}

/**
 * 两位小数验证
 * @param {*} rule
 * @param {*} value
 * @param {*} callback
 */
 const validateValidity = (rule, value, callback) => {
  if (!/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/.test(value)) {
    callback(new Error('最多两位小数！！！'));
  } else {
    callback();
  }
};

/**
 * 判断是否为空
 */
 export function isNull(val) {
  if (val instanceof Array) {
    if (val.length === 0) return true
  } else if (val instanceof Object) {
    if (JSON.stringify(val) === '{}') return true
  } else {
    if (val === 'null' || val == null || val === 'undefined' || val === undefined || val === '') return true
    return false
  }
  return false
}

/*
 * 字符验证，只能包含中文、英文、数字、下划线等字符。
 * 只能包含中文、英文、数字、下划线等字符
 */
export function stringCheck(value) {
  return /^[a-zA-Z0-9\u4e00-\u9fa5-_]+$/.test(value)
}
