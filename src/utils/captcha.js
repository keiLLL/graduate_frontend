/**生成验证码*/

const CODE = '123456789qertadfghjbnmQERTYUPADFKLM'
const LEN = 4 //生成验证码长度
export const makeCaptcha = () => {
    // 生成随机数
    function randomNum(min, max) {
        return Math.floor(Math.random() * (max - min) + min)
    }

    let identifyCode = ''
    for (let i = 0; i < LEN; i++) {
        identifyCode += CODE[
            randomNum(0, CODE.length)
        ]
    }
    // console.log('生成的验证码 ' + identifyCode)
    return identifyCode
}