import Cookies from 'js-cookie'
import store from '@/store'
import { TOKEN, PUBLICKEY, ACCOUNTID } from '@/store/storage'
/** 
 * @description: 按钮权限
 * @param {*} key 
 * @return {*}
 */
export function hasPermission(key) {
    // if (window.SITE_CONFIG['permissions'].indexOf(key) !== -1){
    //     this.$message.error('没有权限');
    // }
    return window.SITE_CONFIG['permissions'].indexOf(key) !== -1 || false
}
/**
 * 获取字典数据列表
 * @param dictType  字典类型
 */
export function getDictDataList(dictType) {
    const type = window.SITE_CONFIG['dictList'].find((element) => (element.dictType === dictType))
    if (type) {
        return type.dataList
    } else {
        return []
    }
}
/**
 * 获取字典名称
 * @param dictType  字典类型
 * @param dictValue  字典值
 */
export function getDictLabel(dictType, dictValue) {
    const type = window.SITE_CONFIG['dictList'].find((element) => (element.dictType === dictType))
    if (type) {
        const val = type.dataList.find((element) => (element.dictValue === dictValue + ''))
        if (val) {
            return val.dictLabel
        } else {
            return dictValue
        }
    } else {
        return dictValue
    }
}

/**
 * 获取svg图标(id)列表
 */
export function getIconList() {
    let res = []
    let list = document.querySelectorAll('svg symbol')
    for (let i = 0; i < list.length; i++) {
        res.push(list[i].id)
    }

    return res
}
/** 
 * @description: 清除登录信息:token+vuex+userId
 * @return {*}
 */
export function clearLoginInfo() {
    // Cookies.remove(TOKEN)
    sessionStorage.removeItem(TOKEN)
    sessionStorage.removeItem(ACCOUNTID)
    // sessionStorage.removeItem(PUBLICKEY)
    // sessionStorage.clear()
    store.commit('resetStore')
    window.SITE_CONFIG['dynamicMenuRoutesHasAdded'] = false
}

/**
 * 树形数据转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */
export function treeDataTranslate(data, id = 'id', pid = 'pid') {
    let res = []
    let temp = {}
    for (let i = 0; i < data.length; i++) {
        temp[data[i][id]] = data[i]
    }
    for (let k = 0; k < data.length; k++) {
        if (!temp[data[k][pid]] || data[k][id] === data[k][pid]) {
            res.push(data[k])
            continue
        }
        if (!temp[data[k][pid]]['children']) {
            temp[data[k][pid]]['children'] = []
        }
        temp[data[k][pid]]['children'].push(data[k])
        data[k]['_level'] = (temp[data[k][pid]]._level || 0) + 1
    }
    return res
}

/** 
 * 自适应
 */
 export function getBodyHeight() {//动态获取浏览器高度
    const that = this
    window.onresize = () => {
        return (() => {
            window.fullHeight = document.documentElement.clientHeight
            that.fullHeight = window.fullHeight
        })()
    }
}