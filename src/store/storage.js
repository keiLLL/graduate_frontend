// const LOGININFO = 'LOGININFO'
const USERROLE = 'USERROLE'
const TOKEN = 'csrfToken'
const PUBLICKEY = 'PublicKey'
const ACCOUNTID = 'ACCOUNTID'
const USERNAME = 'userName'
const ENCODEPASSWORD = 'encodePwd'
export { TOKEN, PUBLICKEY, ACCOUNTID }
import Cookies from 'js-cookie'
import { desDecodeCrtpto, desEncryptCrtpto } from '@/utils/crypto'; //对称加密
const KEY = 'asdfghjbakvveyid17263t1$^*Ot71' //加密密钥
const Base64 = require('js-base64').Base64

const storageService = {
  setRmbPwd: function(obj) {//加密
    let username = obj.username
    let password = Base64.encode(obj.password) // Base64加密
    // console.log('Base64加密：%s', password)
    password = desEncryptCrtpto(password, KEY) // Des加密
    // console.log('Des加密：%s', password)
    let expires = 7 //过期时间7天 默认支持：天
    // let expires = new Date(new Date() * 1 + 7 * 3600 * 1000) //设置更小的时间单位：毫秒
    Cookies.set(USERNAME, username, { path: '/', expires: expires, sameSite: 'Strict' }) //js-cookie会将传入数据用JSON.stringify转换为string保存。 
    Cookies.set(ENCODEPASSWORD, password, { path: '/', expires: expires, sameSite: 'Strict' }) //, 
    //JSON.stringify 方法将某个对象转换成 JSON 字符串形式
  },
  getRmbPwd: function() {//解密
    let obj = {
      username: '',
      password: ''
    }
    obj.username = Cookies.get(USERNAME)//js-cookie会用JSON.parse解析string并返回对象
    let Pwd = Cookies.get(ENCODEPASSWORD)
    // console.log(obj.username)
    // console.log(Pwd)
    if (Pwd == undefined) { //没有记住密码时直接不进行下面的解密
      return null
    }
    Pwd = desDecodeCrtpto(Pwd, KEY) //Des解密
    // console.log('Des解密：%s', Pwd)
    obj.password = Base64.decode(Pwd) //Base64解密
    // console.log('Base64解密：%s', obj.password)
    // console.log(obj)
    return obj
    //JSON.parse将JSON字符串转为对象；
  },
  removeRmbPwd: function() {
    Cookies.remove(USERNAME)
    Cookies.remove(ENCODEPASSWORD) // { path: '/', sameSite: 'Lax' }
  },
  getToken: function() { //获取token
    return sessionStorage.getItem(TOKEN)
  },
  setToken: function(val) { //设置token
    sessionStorage.setItem(TOKEN, val)
  },
  getTokenName() {
    return TOKEN
  },  
  removeToken: function() { // 移除token
    sessionStorage.removeItem(TOKEN)
  },

  setUserRole: function(val) {
    sessionStorage.setItem(USERROLE, val)
  },
  getUserRole: function() {
    return sessionStorage.getItem(USERROLE)
  },

  setAccountId: function(val) {
    sessionStorage.setItem(ACCOUNTID, val)
  },

  getAccountId: function() {
    return sessionStorage.getItem(ACCOUNTID)
  }
}

export default storageService
