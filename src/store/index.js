import Vue from 'vue'
import Vuex from 'vuex'
import cloneDeep from 'lodash/cloneDeep'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  state: { //保存应用状态的容器
    // 导航条, 布局风格, default(白色) / colorful(鲜艳)
    navbarLayoutType: 'colorful',
    // 侧边栏, 布局皮肤, default(白色) / dark(黑色)
    sidebarLayoutSkin: 'dark',
    // 侧边栏, 折叠状态
    sidebarFold: false,
    // 侧边栏, 菜单
    sidebarMenuList: [],
    sidebarMenuActiveName: '',
    // 内容, 是否需要刷新
    contentIsNeedRefresh: false,
    // 内容, 标签页(默认添加首页)
    contentTabs: [
      {
        ...window.SITE_CONFIG['contentTabDefault'],
        'name': 'home',
        'title': 'home'
      }
    ],
    contentTabsActiveName: 'home',
  },
  getters: { //store的计算属性
    // sellingBooks: state => state.books.filter(book => book.isSold===true),
    // sellingBooksCount: (state,getters) => {
    //   return getters.sellingBooks.length
    // }
    // getBookById: state => id => state.books.find( book => book.id === id)
  },
  mutations: { //改变store中状态的唯一途径：显示提交mutation
    // SET_TOKEN: (state, token) => {
    //   state.token = token
    // },
    // SET_STATE: (state, payload) => {
    //   state = payload
    // },
    updateNavbarLayoutType(state, type) {
      state.navbarLayoutType = type
    },
    updateSidebarLayoutSkin(state, skin) {
      state.sidebarLayoutSkin = skin
    },
    // 重置vuex本地储存状态
    resetStore(state) {
      Object.keys(state).forEach((key) => {
        state[key] = cloneDeep(window.SITE_CONFIG['storeState'][key])
      })
    }
  },
  actions: { //mutations必须为同步函数,action提交的事mutation->允许异步操作
    // Login({
    //   commit
    // }, userInfo) {
    //   return new Promise((resolve, reject) => {
    //     login(userInfo.account, userInfo.password).then(x => {
    //       if (x.status == 200) {
    //         const tokenV = x.data.token.tokenValue
    //         commit('SET_TOKEN', tokenV)
    //         document.cookie = `AuthInfo=Bearer ${tokenV};path:/`;
    //         token = "Bearer " + tokenV;
    //         //setToken("Bearer " +token)
    //         resolve();
    //       }

    //     }).catch(error => {
    //       // console.log("登录失败")
    //       reject(error)
    //     })
    //   })
    // },
  },
  modules: {
    user
  }
})
