import axios from 'axios'
import { Message } from 'element-ui'
import storageService from '@/store/storage'
import { clearLoginInfo } from '@/utils';
import router from '@/router'


const apiHost = 'http://localhost:8082' //服务器
// const apiHost = window.SITE_CONFIG['apiURL'] //配置生产和开发路径
// console.log(apiHost)
export {apiHost}
import NProgress from 'nprogress'

// 这是进度条的一些配置项
NProgress.configure({
  easing: 'speed',  // 动画方式
  speed: 500,  // 递增进度条的速度
  showSpinner: false, // 是否显示加载ico
  trickleSpeed: 200, // 自动递增间隔
  minimum: 0.3 // 初始化时的最小百分比
});

const axiosService = {
  axios: axios.create({
    baseURL: apiHost,
    timeout: 1000 * 60, // 请求超时限时时间 60S
    withCredentials: true, //设置axios请求携带凭证信息（cookie），默认false
    retry: 1, //设置全局重试请求次数（最多重试几次请求）
    retryDelay: 1000, //设置全局请求间隔
    // transformRequest:[ function(data,headers){ //允许在讲请求数据发送到服务器前对其进行修改
    //   //这里可以进行数据加密
    //   return data
    // }],
    // transformResponse: [function (data) { //允许在讲响应数据传递给try/catch之前对起进行更改
    //   //这里可以进行数据解密
    //   return data
    // }],
    // cancelToken: source.token
    // signal: controller.signal
  })
}
/**
 * 请求拦截
 */
axiosService.axios.interceptors.request.use(config => {
  // 进度条引用开始
  NProgress.start()

  // const TOKEN = 'csrfToken'
  // 1.从session中获取token的值
  // let token = sessionStorage.getItem(TOKEN);
  // 2.从cookie中获取token的值
  // let Token = Cookies.get(TOKEN);
  // 3.从local Storage中获取token的值
  // let token = window.localStorage.getItem(TOKEN);

  // 4.封装 token
  const TOKEN = storageService.getTokenName()
  const token = storageService.getToken()
  // 在请求前统一添加 Token 的 header 信息
  if (token) {
    config.headers[TOKEN] = token
  }
  // config.cancelToken = source.token
  // // console.log(config)
  return config
}, error => {
  // console.log('请求错误：' + JSON.stringify(error))
  // Message.error('错误：' + error)
  Promise.reject(error)
})

/**
 * 响应拦截
 */
axiosService.axios.interceptors.response.use(
  response => {
    // 进度条引用结束
    NProgress.done()
    if (response.status === 200 && response.data.code === 401) {
      router.replace({ path: '/login' });
      clearLoginInfo()
      return Promise.reject(response.data.msg)
    }
    // console.log('后端响应成功')
    return response
  }, error => {
    NProgress.done()
    //超时处理 error.config是一个对象，包含上方create中设置的三个参数
    let config = error.config
    // source.cancel()
    // controller.abort()
    // console.log('后端响应错误')
    // console.log('response错误：' + JSON.stringify(error.config))
    if (error.config.url == '/common/noticefile' || error.config.url == '/student/wenxian' || error.config.url == '/student/paper' || error.config.url == '/common/update/notices') {
      Message.error('请求超时啦，请检查网络')
      return Promise.reject({ type: 'error', msg: '请求超时啦，请检查网络' });
    }
    // //     Message.error('错误：' + error)
    // //     return Promise.reject(error)
    if (error.response == undefined) { //请求超时或者后端没开启
      // Message.error('请求超时啦，网络错误')
      // 取消请求（message 参数是可选的）
      // source.cancel('Operation canceled by the user.');
      // __retryCount用来记录当前是第几次发送请求
      config.__retryCount = config.__retryCount || 0;
      // 如果当前发送的请求大于等于设置好的请求次数时，不再发送请求，返回最终的错误信息
      if (config.__retryCount >= config.retry) {
        if (error.message === 'Network Error') {
          //message为"Network Error"代表断网了
          Message.error('网络连接已断开，请检查网络')
          return Promise.reject({ type: 'warning', msg: '网络连接已断开，请检查网络' });
        } else if (error.message === 'timeout of 6000ms exceeded') {
          //网太慢了，15秒内没有接收到数据，这里的6000ms对应上方timeout设置的值
          Message.error('请求超时，请检查网络')
          return Promise.reject({ type: 'warning', msg: '请求超时，请检查网络' });
        } else {
          //除以上两种以外的所有错误，包括接口报错 400 500 之类的
          Message.error('网络出现错误，请稍后再试')
          return Promise.reject({ type: 'error', msg: '出现错误，请稍后再试' });
        }
      }

      // 记录请求次数+1
      config.__retryCount += 1;
      // console.log(config.__retryCount)
      // 设置请求间隔 在发送下一次请求之前停留一段时间，时间为上方设置好的请求间隔时间
      let backoff = new Promise(function(resolve) {
        setTimeout(function() {
          resolve();
        }, config.retryDelay || 1);
      });

      // 再次发送请求
      return backoff.then(function() {
        // console.log('AAAAAAAAAAA')
        return axiosService.axios(config);
      });
      // return Promise.reject('请求超时')
    } else if (error.response.status) {
      // console.log(error.response.status)
      switch (error.response.status) {
          // 401: 未授权 未登录
          // 未登录则跳转登录页面，并携带当前页面的路径
          // 在登录成功后返回当前页面，这一步需要在登录页操作。
          case 401:
            Message.error('请登录')
            // （vue中的this.$router 和 this.$store都是在vue组件中使用才有效，也就是.vue文件，js文件的话，需要引入才能使用，this.$store也是需要引入才能用哦）
            router.replace({path: '/login'});
            clearLoginInfo()
            break;
          // 403  token过期
          // 登录过期对用户进行提示
          // 清除本地token和清空vuex
          // 跳转登录页面
          case 403:
            Message.warning('登录过期，请重新登录')
            // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面 
            router.replace({  name: 'login' }); 
            // 清除登录信息
            clearLoginInfo()
            break;
          // 404 	服务器找不到请求的网页
          case 404:
            Message.error('服务器找不到请求的网页') 
            // Message.error('请求路径错误') 
            router.replace({ name: '404' })
            break;
          // 405 	请求方法错误
          case 405:
            Message.error('请求方法错误')
            // router.replace({ name: '404' })
            break;
          // 500 内部服务器错误
          case 500:
            Message.warning('内部服务器错误')
            router.replace({ name: 'login' })
            clearLoginInfo()
            break;
          // 其他错误，直接抛出错误提示
          default:
            Message.error('出错了')
      }
      return Promise.reject(error.response);
    }
  }
,);
export default axiosService
