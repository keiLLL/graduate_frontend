// import { parseBigInt } from 'jsencrypt/lib/lib/jsbn/jsbn'
import axiosService from './axiosInterceptor'
import qs from 'qs'
// GET （查询用户）

// POST （新增用户）
// PUT （更新用户）
// DELETE （删除用户）
// PUTCH 更新用户部分信息，一般不用
const servicesApi = {
  // login-controller----------------------------------
  getAllDict() { //获取所有数据字典
    const url = '/login/all/dicts'
    return axiosService.axios.get(url)
  },
  getBtnPermissions() { //获取所有按钮权限
    const url = '/login/btons'
    return axiosService.axios.get(url)
  },
  getTeaList() { //拿到登录用户所在学院的所有老师
    const url = '/login/department/teacher'
    return axiosService.axios.get(url)
  },
  login(data) {  //登录
    // console.log(data)
    const url = '/login/user'
    return axiosService.axios.post(url, data)
  },
  getMenuNav() { //菜单列表
    const url = '/login/login/perms'
    return axiosService.axios.post(url)
  },
  logout() {  //登出
    // console.log('退出登录')
    const url = 'login/logout'
    return axiosService.axios.post(url)
  },
  phoneLogin(data) { //手机号登录
    // console.log(data)
    const url = '/login/phone'
    return axiosService.axios.post(url, data)
  },
  getRsaPublicKey() { //获取公钥
    const url = '/login/public'
    return axiosService.axios.get(url)
  },
  getNoticeList(data) {//ES模糊查询公告
    // console.log(data)
    let form = {
      current: '',
      name: data,
      size: '0'
    }
    const url = `/common/esnotice1?keyword=${data}`
    return axiosService.axios.get(url)
  },
  getDepInfo() { //拿到登录用户所在学院的所有老师人数，学生人数
    const url = '/login/student/teacher/number'
    return axiosService.axios.get(url)
  },
  
  // Common Controller----------------------------------
  //公告管理----------------------------------
  getHomeNotice() { //获取首页公告
    const url = '/common/all/notices/perms'
    return axiosService.axios.get(url)
  },
  getNoticeAll() { //初始化页面获取所有公告
    const url = '/common/all/notices'
    return axiosService.axios.get(url)
  },
  getActiveStepApi() { //进度
    const url = '/common/all/activestep'
    return axiosService.axios.get(url)
  },
  /** 
   * @description: 确认账号是否存在
   * @param {*} username 
   * @return {*}
   */
  checkUsername(username) {
    const url = `/common/check/${username}`
    return axiosService.axios.get(url)
  },
  delNoticeFileById(fileId) { //删除公告-附件
    // console.log(fileId)
    const url = `/common/delete//${fileId}/noticefile`
    return axiosService.axios.post(url)
  },
  /** 
   * @description: 删除公告
   * @param {*} data 
   * @return {*}
   */
  deleteNoticeByIds(data) {//
    let form = {
      list: data
    }
    // console.log(data)
    const url = '/common/delete/notices'
    return axiosService.axios.post(url, form)
  },
  /** 
   * @description: 获取公告详情
   * @param {*} id 
   * @return {*}
   */
  getNoticeInfoById(id) {//
    // console.log(id)
    const url = `/common/notice/${id}/info`
    return axiosService.axios.get(url)
  },
    /** 
   * @description: 提供公告浏览时间
   * @param {*} id 
   * @return {*}
   */
     postNoticeInfoById(data) {//
      // console.log(data)
      const url = '/common/notice/info'
       return axiosService.axios.post(url, {}, { params: { msg: data.msg}})
    },
  addNoticeFile(data) { //上传公告文件
    // console.log(data)
    const url = '/common/noticefile'
    return axiosService.axios.post(url, data, {
      headers: { 'Content-Type': 'multipart/form-data' } //设置请求头请求格式为JSON
    })
  },
  addNotice(data) { //新增公告
    // console.log(data)
    const url = '/common/notices'
    return axiosService.axios.post(url, data)
  },
  getCode(phone) { //发送短信
    // console.log('手机号为=%s', phone)
    const url = `/common/send/${phone}`
    return axiosService.axios.get(url)
  },

  /** 
   * @description: 修改公告
   * @param {*} data 
   * @return {*}
   */
  updateNotice(data) {//
    // console.log(data)
    const url = '/common/update/notices'
    return axiosService.axios.post(url, data, {
      headers: { 'Content-Type': 'multipart/form-data' } //设置请求头请求格式为JSON
    })
  },
  resetPwd(data) { //重置密码
    let pwdForm = {
      username: data.account,
      password: data.comfirmPassword,
      phone: data.phone
    }
    // console.log(pwdForm)
    const url = '/common/update/password'
    return axiosService.axios.post(url, pwdForm)

    // const url = `/common/updatePassword`
    // return axiosService.axios.post(url,qs.stringify({newpass: data.confirmPwd}))
  },
  getNoticeList(data) {//ES模糊查询公告
    // console.log(data)
    let form = {
      current: '',
      name: data,
      size: '0'
    }
    let es = encodeURI(encodeURI(data))
    // console.log(es)
    // const url = `/common/esnotice1?keyword=${es}` encodeURI(data)
    return axiosService.axios.get('/common/esnotice1', { params: { keyword: data }})
  },

  
  


  // ========管理员端=======================================
  //用户管理----------------------------------
  getFlagList() { //获取身份列表
    // const url = `/admin/addUser`
    // return axiosService.axios.post(url)
  },
  addUser(data) { //添加用户
    // console.log(data)
    const url = '/admin/users'
    return axiosService.axios.post(url, data)
  },
  deleteUsersById(data) { //删除单个或多个用户
    // console.log(data)
    let list = qs.stringify(data, { indices: false })
    // console.log(form)

    const url = `/admin/delete/users?${list}`
    return axiosService.axios.delete(url)
  },

  updateUserA(data) {//修改用户信息-管理员
    // console.log(data)
    const url = '/admin/update/admins'
    return axiosService.axios.post(url, data)
  },
  updateUserT(data) {//修改用户信息-教师
    // console.log(data)
    const url = '/admin/update/teachers'
    return axiosService.axios.post(url, data)
  },
  updateUserS(data) {//修改用户信息-学生
    // console.log(data)
    const url = '/admin/update/students'
    return axiosService.axios.post(url, data)
  },

  getUserList(data) {// 展示、模糊查询所有用户
    const url = '/admin/show/user' 
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  getUserById(userId) {//查询用户信息ById
    // console.log(userId)
    const url = `/admin/user/${userId}/info`
    return axiosService.axios.get(url)
  },

  //角色管理----------------------------------
  addRole(data) { //添加角色 // Role Controller
    // console.log(data)
    const url = '/role/addRole'
    return axiosService.axios.post(url, data)
  },
  getAllUsers() { //获取所有用户
    const url = '/admin/all/users'
    return axiosService.axios.get(url)
  },
  getUsersSelected(roleId) { // 拿到该角色已有的成员
    // console.log(roleId)
    const url = `/admin/role/${roleId}/users`
    return axiosService.axios.get(url)
  },
  getUsersUnSelected(roleId) { // 拿到该角色没有的成员
    // console.log(roleId)
    const url = `/admin/role/${roleId}/nousers`
    return axiosService.axios.get(url)
  },
  deleteRolesById(data) { //删除单个或多个角色
    let form = qs.stringify(data, { indices: false })
    // console.log(form)

    const url = `/admin/delete/roles?${form}`
    return axiosService.axios.delete(url)
  },
  updateRole(data) {//修改角色信息
    // console.log(data)
    const url = '/admin/update/roles'
    return axiosService.axios.post(url, data)
  },
  getRoleList(data) {//模糊查询所有角色
    const url = '/admin/show/roles'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  getRoleById(roleId) {//查询角色信息ById
    // console.log(roleId)
    const url = `/admin/role/${roleId}/msg`
    return axiosService.axios.get(url)
  },
  setRolePerm(data) { //为角色分配权限
    // console.log(data)
    const url = '/role-perms/setRolePerm'
    return axiosService.axios.post(url, data)
  },
  addUserRole(data) { //把角色分配给用户，成员管理
    // console.log(data)
    const url = '/admin/update/role/users'
    return axiosService.axios.post(url, data)
  },
  // Tree Perm Controller
  getAllPerms() { //查询所有权限 
    const url = '/treeperm/all/perms'
    return axiosService.axios.get(url)
  },
  getRolePerms(roleId) { //查询角色权限
    // console.log(roleId)
    const url = `/admin/perms/${roleId}/role`
    return axiosService.axios.get(url)
  },
  updateRolePerms(data) { //更新角色权限
    // console.log(data)
    const url = '/admin/update/role/perms'
    return axiosService.axios.post(url, data)
  },

  //日志管理----------------------------------
  LogErrorExport() { //异常日志导出Excel
    const url = '/log/excels'
    return axiosService.axios.get(url, { responseType: 'blob'})
  },
  getLogError(data) { //异常日志
    // console.log(data)
    const url = '/log/show/error/log'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  getLogOperation(data) {//操作日志
    // console.log(data)
    const url = '/log/show/operation/log'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  getLoginLog(data) {//登录日志
    // console.log(data)
    const url = '/log/show/login/log'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //字典管理----------------------------------
  /** 
   * @description: 分页获取所有字典
   * @param {*} data 
   * @return {*}
   */
  getDictList(data) { //
    // console.log(data)
    const url = '/admin/show/sysdicttype'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  /** 
   * @description: 删除字典
   * @param {*} data 
   * @return {*}
   */
  deleteDictsByIds(data) {//
    // console.log(data)
    let form = {
      list: data,
    }
    let list = qs.stringify(form, { indices: false })
    // console.log(form)

    const url = `/admin/delete/sysdicttype?${list}`
    return axiosService.axios.delete(url)
  },
  /** 
   * @description: 修改字典
   * @param {*} data 
   * @return {*}
   */
  updateDict(data) {//
    // console.log(data)
    const url = '/admin/update/sysdicttype'
    return axiosService.axios.post(url, data)
  },
  /** 
   * @description: 获取字典详情
   * @param {*} id 
   * @return {*}
   */
  getDictInfoById(id) {//
    // console.log(id)
    const url = `/admin/sysdicttype/${id}/info`
    return axiosService.axios.get(url)
  },
  /** 
   * @description: 新增字典
   * @param {*} data 
   * @return {*}
   */
  addDict(data) {//
    // console.log(data)
    const url = '/admin/sysdicttype'
    return axiosService.axios.post(url, data)
  },
  /** 
   * @description: 分页获取所有字典项
   * @param {*} data 
   * @return {*}
   */
  getDictItemList(data) { //
    // console.log(data)
    const url = '/admin/show/sysdictdata'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  /** 
   * @description: 删除字典数据
   * @param {*} data 
   * @return {*}
   */
  deleteDictItemsByIds(data) {//
    let form ={
      list: data
    }
    // console.log(data)
    let list = qs.stringify(form, { indices: false })
    // console.log(form)

    const url = `/admin/delete/sysdictdata?${list}`
    return axiosService.axios.delete(url)
  },
  /** 
   * @description: 修改字典项
   * @param {*} data 
   * @return {*}
   */
  updateDictItem(data) {
    // console.log(data)
    const url = '/admin/update/sysdictdata'
    return axiosService.axios.post(url, data)
  },
  /** 
   * @description: 获取字典项详情
   * @param {*} id:字典id 
   * @return {*}
   */
  getDictItemInfoById(id) {//
    // console.log(id)
    const url = `/admin/sysdictdata/${id}/info`
    return axiosService.axios.get(url)
  },
  /** 
   * @description: 新增字典项
   * @param {*} data 
   * @return {*}
   */
  addDictItem(data) {
    // console.log(data)
    const url = '/admin/sysdictdata'
    return axiosService.axios.post(url, data)
  },
  //定时任务----------------------------------
  timeTask(data) {
    let timer = parseInt(data.params)
    // let timer = parseBigInt(data.params)
    // console.log(timer)
    // console.log(typeof(timer))
    // console.log('定时时间=%s', timer)
    const url = `/test/update/${timer}`
    return axiosService.axios.get(url)
  },

  //菜单管理----------------------------------
  /** 
   * @description: 获取所有菜单
   * @return {*}
   */
  getMenuList() {
    const url = '/admin/all/menus'
    return axiosService.axios.get(url)
  },
  deleteMenu(permId) { // 删除菜单或按钮
    // console.log('删除菜单/按钮的permId=%s', permId)
    const url = '/admin/delete/menu'
    return axiosService.axios.delete(url, {params: {permId: permId} })
  },
  getMenuById(permId) { // 拿到当前权限的具体信息
    // console.log(permId)
    const url = `/admin/menu/${permId}/info`
    return axiosService.axios.get(url)
  },
  /** 
   * @description: 修改菜单或按钮
   * @param {*} data 
   * @return {*}
   */
  updateMenu(data) {
    // console.log(data)
    const url = '/admin/update/menus'
    return axiosService.axios.post(url, data)
  },
  /** 
   * @description: 新增菜单或按钮
   * @param {*} data 
   * @return {*}
   */
  addMenu(data) {
    // console.log(data)
    const url = '/admin/menu'
    return axiosService.axios.post(url, data)
  },

  
  


  //=====学生端==========================================
  downloadFiles(data) { // 模板下载/文件下载：开题报告、文献、论文
    // console.log(data)
    const url = data.url
    const id =  data.id
    if (id==-1) { //模板下载
      return axiosService.axios.post(url, data.data, { responseType: 'blob' })
    }
    {return axiosService.axios.post(url, {}, { params: { id, }, responseType: 'blob' })}
    // const url = '/teacher/down/file'
    // return axiosService.axios.post(url, data, { responseType: 'blob' })
  },
  /** 
   * @description: 删除自己的上传文献记录，同时删除文件
   * @return {*}
   */
  deleteFile() {
    const url = '/student/delete/wenxian'
    return axiosService.axios.post(url)
  },
  getTopicList(data) { //分页模糊查询所有课题
    // console.log(data)
    const url = '/student/show/categorys'
    return axiosService.axios.post(url, data) 
    // !!!!!!!!!!!!
    // return axiosService.axios.get(url, { params: { ...data } })
  },
  getTopicInfo(cgyId) { // 获取课题详情
    const url = `/student/category/${cgyId}/info`
    return axiosService.axios.get(url)
  },
  /** 
   * @description: 学生申报题目(修改也是这个接口)
   * @param {*} data 
   * @return {*}
   */
  applyTopic(data) {
    // console.log(data)
    const url = '/student/category'
    return axiosService.axios.post(url, data)
  },
  /** 
   * @description: 拿到自己申报的课题
   * @return {*}
   */
  getTopic() {
    const url = '/student/category'
    return axiosService.axios.get(url)
  },
  chooseTopic(cgyId) { //学生选题
    const url = `/student/choose/category/${cgyId}`
    return axiosService.axios.post(url)
  },
  /** 
   * @description: 删除自己申报的课题
   * @return {*}
   */
  delSelfTopic() {
    const url = '/student/delete/category'
    return axiosService.axios.delete(url)
  },
  /** 
   * @description: 删除自己选择的课题
   * @return {*}
   */
  delTopic() {
    const url = '/student/delete/category/choose'
    return axiosService.axios.post(url)
  },
  /** 
   * @description: 展示自己选择的题目
   * @return {*}
   */
  getSelectedTopic() {
    const url = '/student/category/choose'
    return axiosService.axios.get(url)
  },

  /** 
   * @description:  提交开题报告(提交和修改都是这一个接口)
   * @param {*} data form
   * @return {*}
   */
  submitReport(data) {
    // console.log(data)
    const url = '/student/report'
    return axiosService.axios.post(url, data)
  },
  /** 
   * @description: 展示自己的开题报告
   * @return {*}
   */
  getReport() {
    const url = '/student/show/reports'
    return axiosService.axios.get(url)
  },

  /** 
   * @description: 删除自己的开题报告
   * @return {*}
   */
  delReport() {
    const url = '/student/delete/report'
    return axiosService.axios.delete(url)
  },
  /**
   * @description: 提交文献(提交和修改都是这一个接口)
   * @param {String} describe  文件描述
   * @param {File} file 文件
   * @return {*}
   */
  submitWenxian(file) {
    // // console.log(describe)
    const url = '/student/wenxian'
    return axiosService.axios.post(url, file)
  },

  submitmidreport(file) {
    // // console.log(describe)
    const url = '/teacher/uploadInspectionReport'
    return axiosService.axios.post(url, file)
  },
  submitmiddreport(file) {
    // // console.log(describe)
    const url = '/teacher/getInspectionReportUrl'
    return axiosService.axios.post(url, file)
  },

  /** 
   * @description: 展示自己的上传文献记录
   * @return {*}
   */
  getWenxian() {
    const url = '/student/show/wenxian'
    return axiosService.axios.get(url)
  },
  /** 
   * @description: 删除自己的上传文献记录，同时删除文件
   * @return {*}
   */
  delWenxian() {
    const url = '/student/delete/wenxian'
    return axiosService.axios.delete(url)
  },

  //提交指导记录
  recordForm(data) {
    // console.log(data)
    const url = '/student/records'
    return axiosService.axios.post(url, data)
  },
  //删除指导记录
  delGuidance(data) {
    let list = qs.stringify(data, { indices: false })
    // console.log(list)

    const url = `/student/delete/records?${list}`
    return axiosService.axios.delete(url)
  },
  //修改指导记录
  changeGuidance(data) {
    // console.log(data)
    const url = '/student/update/records'
    return axiosService.axios.post(url, data)
  },
  //拿到指导记录
  getGuidance(data) {
    const url = '/student/show/records'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //提交论文----------------
  submitDesign(file) {
    // // console.log(describe)
    const url = '/student/paper'
    return axiosService.axios.post(url, file)
  },
  /** 
   * @description: 展示自己的上传论文
   * @return {*}
   */
  getDesign() {
    const url = '/student/paper'
    return axiosService.axios.get(url)
  },
  /** 
   * @description: 删除自己的上传论文记录，同时删除文件
   * @return {*}
   */
  //删除论文
  deleteDFile() {
    const url = '/student/delete/paper'
    return axiosService.axios.delete(url)
  },
  //查看成绩----------------------------------
  getfindgradeApi() {
    const url = '/student/find'
    return axiosService.axios.get(url)
  },
   //学生成绩查询----------------------------------
   stufindgradeApi() {
    const url = '/student/grade'
    return axiosService.axios.get(url)
  },

   //学生中期答辩安排----------------------------------
   stuzhonqifindarrangeApi() {
    const url = '/student/midtermDefence'
    return axiosService.axios.get(url)
  },

  //学生期末答辩安排----------------------------------
  stufindarrangeApi() {
    const url = '/student/defence'
    return axiosService.axios.get(url)
  },

  //学生个人管理----------------------------------
  editstudentmanageApi(data) {
    // console.log(data)
    const url = '/student/update/self/info'
    return axiosService.axios.post(url, data)
  },
  //==============教师端=======================
  //模糊查询开题报告
  getTReport(data) {
    // console.log(data)
    const url = '/teacher/show/report'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //审核开题报告
  checkTreport(data) {
    // console.log(data)
    const url = '/teacher/batch/check/report'
    return axiosService.axios.post(url, data)
  },
  //查看详细开题报告
  checkTdreport(id) {
    const url = `/teacher/report/${id}/info`
    return axiosService.axios.get(url)
  },
  //删除开题报告
  delTreport(data) {
    // console.log(data)
    let list = qs.stringify(data, { indices: false })
    // console.log(list)

    const url = `/teacher/delete/report?${list}`
    return axiosService.axios.delete(url)
  },
  //模糊查询指导记录
  getTguidance(data) {
    // console.log(data)
    const url = '/teacher/show/records/student'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //删除指导记录
  delTguidance(data) {
    // console.log(data)
    const url = '/teacher/delete/records'
    return axiosService.axios.post(url, data)
  },
  //查看详细指导记录  
  checkTdguidance(id) {
    const url = `/teacher/records/${id}/info`
    return axiosService.axios.get(url)
  },
  //院系老师模糊查询指导记录
  getTTguidance(data) {
    // console.log(data)
    const url = '/teacher/show/all/records'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //院系老师删除指导记录
  delTTguidance(data) {
    // console.log(data)
    let list = qs.stringify(data, { indices: false })
    // console.log(list)

    const url = `/teacher/delete/records/student?${list}`
    return axiosService.axios.delete(url)
  },
  //老师获取学生选题
  getTmguidance(data) {
    // console.log(data)
    const url = '/teacher/show/final/choose'
    return axiosService.axios.get(url, {params: data})
  },
  //修改自己的指导记录
  changeTGuidance(data) {
    // console.log(data)
    const url = '/teacher/update/records'
    return axiosService.axios.post(url, data)
  },
  //提交自己指导记录
  submitTguidance(data) {
    // console.log(data)
    const url = '/teacher/records'
    return axiosService.axios.post(url, data)
  },
  //查看自己为学生填写的指导记录
  getTrguidance(data) {
    const url = '/teacher/show/records'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //教师查询自己申报的课题
  getTTtopic(data) {
    const url = '/teacher/show/my/categorys'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //老师申报课题
  submitTtopic(data) {
    const url = '/teacher/category'
    return axiosService.axios.post(url, data)
  },
  //老师修改课题
  changeTtopic(data) {
    const url = '/teacher/update/category'
    return axiosService.axios.post(url, data)
  },
  //删除申报课题
  delTtopic(data) {
    // console.log(data)
    let list = qs.stringify(data, { indices: false })
    // console.log(list)

    const url = `/teacher/delete/my/categorys?${list}`
    return axiosService.axios.delete(url)
  },
  //教师查看详细
  getTdtopic(cgyid) {
    const url = `/teacher/category/${cgyid}/info`
    return axiosService.axios.get(url)
  },
  //审核选择我的课题的学生
  checkTStopic(data) {
    // console.log(data)
    const url = '/teacher/batch/check/choose'
    return axiosService.axios.post(url, data)
  },
  //获取选择我的课题的学生
  getTStopic(data) {
    const url = '/teacher/show/choose'
    return axiosService.axios.get(url, {params: {...data}})
  },
  //删除选择我的课题的学生
  delTStopic(data) {
    // console.log(data)
    let list = qs.stringify(data, { indices: false })
    // console.log(list)

    const url = `/teacher/delete/choose?${list}`
    return axiosService.axios.delete(url)
  },
  //获取审核学生申报课题
  getTSchecktopic(data) {
    const url = '/teacher/show/categorys'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.post(url, {}, { params: { ...data } })
  },
  //删除审核学生申报的课题
  delTSchecktopic(data) {
    // console.log(data)
    let list = qs.stringify(data, { indices: false })
    // console.log(list)

    const url = `/teacher/delete/category/student?${list}`
    return axiosService.axios.delete(url)
  },
  //审核学生申报的课题
  checkTSchecktopic(data) {
    // console.log(data)
    const url = '/teacher/batch/check/category'
    return axiosService.axios.post(url, data)
  },
  //院系老师获取申报课题
  getTTSchecktopic(data) {
    const url = '/teacher/show/all/category'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //院系老师审核申报课题
  checkTTSchecktopic(data) {
    // console.log(data)
    const url = '/teacher/batch/check/category/leader'
    return axiosService.axios.post(url, data)
  },
  //获取文献
  getTwenx(data) {
    const url = '/teacher/show/wenxian'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //审核文献
  checkTwenx(data) {
    const url = '/teacher/check/wenxian'
    return axiosService.axios.post(url, data)
  },
 

  //毕业论文审核----------------------------------
  //请求论文列表
  postpaperApi(data) {
    const url = '/teacher/show/papers'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //批量审核
  checkpaperApi(data) {
    const url = '/teacher/batch/check/paper'
    return axiosService.axios.post(url, data)
  },

  //分页查看老师答辩安排----------------------------------
  teafindarrangeApi(data) {
    const url = '/teacher/show/defence/self'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //院系期中安排答辩----------------------------------
  yuanmidfindarrangeApi(data) {
    const url = '/teacher/show/midtermDefences'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  //删除期中答辩安排
  delmidTarrange(data) {
      // console.log(data)
      let list = qs.stringify(data, { indices: false })
      // console.log(list)
      const url = `/teacher/delete/midtermDefence?${list}`
      return axiosService.axios.delete(url)
   },
  //修改期中答辩安排
  submitmidTarrange(data) {
    const url = '/teacher/update/midtermDefence'
    return axiosService.axios.post(url, data)
  },
  //新增期中答辩安排
  submitmidTarrange(data) {
    const url = '/teacher/midtermDefence'
    return axiosService.axios.post(url, data)
  },
  //查看详细期中答辩安排
  getTmidarrange(id) {
      const url = `/teacher/midtermDefence/${id}/info`
      return axiosService.axios.get(url)
    },

  //院系期末安排答辩
  yuanfindarrangeApi(data) {
    const url = '/teacher/show/defences'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  yuaneditarrangeApi(data) {
    const url = '/teacher/update/defence'
    return axiosService.axios.post(url, data)
  },
  //查看详细期末答辩安排
  getTarrange(id) {
    const url = `/teacher/defence/${id}/info`
    return axiosService.axios.get(url)
  },
  //新增期末答辩安排
  submitTarrange(data) {
    const url = '/teacher/defence'
    return axiosService.axios.post(url, data)
  },
  //修改期末答辩安排
  submitCTarrange(data) {
    const url = '/teacher/update/defence'
    return axiosService.axios.post(url, data)
  },
   //删除期末答辩安排
   delTarrange(data) {
     // console.log(data)
     let list = qs.stringify(data, { indices: false })
     // console.log(list)
     const url = `/teacher/delete/defence?${list}`
     return axiosService.axios.delete(url)
  },
  //成绩录入----------------------------------
  //获取成绩
  teagetgradeenterApi(data) {
    const url = '/teacher/show/all/grades'
    // return axiosService.axios.post(url, data)
    return axiosService.axios.get(url, { params: { ...data } })
  },
  editgradeenterApi(data) {
    const url = '/teacher/grade'
    return axiosService.axios.post(url, data)
  },

  //教师个人管理----------------------------------
  editteachermanageApi(data) {
    const url = '/teacher/update/self/info'
    return axiosService.axios.post(url, data)
  },

}
export default servicesApi
