// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import ELEMENT from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '@/icons'
import '@/element-ui/theme/index.css'
import '@/assets/scss/aui.scss'
import { hasPermission} from '@/utils' //权限判断
import cloneDeep from 'lodash/cloneDeep' //深拷贝
import servicesAPI from '@/services/servicesApi';
import 'nprogress/nprogress.css' //进度条样式
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
Vue.use(mavonEditor) //markdown富文本
Vue.config.productionTip = false
Vue.use(ELEMENT);
 
// //引入vue懒加载
// import VueLazyload from 'vue-lazyload'
// //方法一：  没有页面加载中的图片和页面图片加载错误的图片显示
// // Vue.use(VueLazyload)

// //方法二：  显示页面图片加载中的图片和页面图片加载错误的图片
// //引入图片
// import loading from '@/assets/img/login_bg.jpg'
// //注册图片懒加载  
// Vue.use(VueLazyload, {
//   // preLoad: 1.3,
//   error: '@/assets/images/error.jpg', //图片错误的替换图片路径(可以使用变量存储)
//   loading: loading, //正在加载的图片路径(可以使用变量存储)
//   // attempt: 1
// })

// 挂载全局
Vue.prototype.$hasPermission = hasPermission
Vue.prototype.$servicesApi = servicesAPI

// 保存整站vuex本地储存初始状态
window.SITE_CONFIG['storeState'] = cloneDeep(store.state)

// Vue.config.errorHandler = function (err) {
//   // console.log("global", err);
//   message.error("出错了");
// };

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
